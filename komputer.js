/*-----------------------------LOAN PART----------------------------*/

let balance = 200;
let currEarned = 0;
let outstandingLoan = 0;
let existingLoan = false;
let textAmount = document.getElementById("txtAmount");
let textLoanAmount = document.getElementById("txtLoanAmount");
let textLoan = document.getElementById("txtLoan");
let btnRepay = document.getElementById("repayBtn");
const laptopPrice = document.getElementById("laptopPrice");
let textSalary = document.getElementById("txtSalary");

//Reusable function to set amounts with sek
const setAmount = (amount, txtId) => {
    const balanceAmount = new Intl.NumberFormat("sv-SE", { style: "currency", "currency":"SEK", maximumFractionDigits: 0, 
    minimumFractionDigits: 0}).format(amount);
    txtId.innerText = balanceAmount;
}

setAmount(balance, textAmount);

//loan button event
loanBtn.addEventListener("click", event => {
    let loanInput = prompt("Enter loan amount");
    if(loanInput === null || loanInput === ""){
        return;
    }else{
        getLoan(parseInt(loanInput));
    }
});

//loan logic, will only accept loan if existingLoan is false and if loan is lower than or equal to twice the balance
const getLoan = (loan) => {
    if (!existingLoan){
        if (loan > (balance*2)){
            alert("You can't get a loan more than double of your bank balance");
        }else{
            balance += loan;
            setAmount(balance, textAmount);
            existingLoan = true;
            outstandingLoan += loan;
            textLoanAmount.style.visibility = "visible";
            textLoan.style.visibility = "visible";
            setAmount(outstandingLoan, textLoanAmount);
            btnRepay.style.visibility = "visible";
        }
    }
    else{
        alert("You already have a loan, you need to repay it before being able to get another one.");
    }
} 

//Function to pay off loan 
const payLoan = () => {
    if(currEarned >= outstandingLoan){
        currEarned -= outstandingLoan;
        outstandingLoan = 0;
        existingLoan = false;
        if(currEarned > 0){
            transferMoney();
        }
    }else {
        outstandingLoan -= currEarned;
        currEarned = 0;
    }
    setAmount(currEarned, textSalary);
    setAmount(outstandingLoan, textLoanAmount);
}


/*----------------------------WORK PART-------------------------------*/


setAmount(currEarned, textSalary);

//Transfer money to bank, if there is outstanding loan then 10% is deducted and transferred to outstanding loan before transfer
const transferMoney = () => {
    if(outstandingLoan > 0){
        let interest = currEarned * (10 / 100);
        outstandingLoan += interest;
        setAmount(outstandingLoan, textLoanAmount);
        currEarned -= interest;
    }
    balance += currEarned;
    currEarned = 0;
    setAmount(balance, textAmount);
    setAmount(currEarned, textSalary);
}

//increases pay with 100
const increaseSalary = () => {
    currEarned +=  100;
    setAmount(currEarned, textSalary);
}

//loan button event
bankBtn.addEventListener("click", transferMoney);

//work button event
workBtn.addEventListener("click", increaseSalary);

//repay button event
repayBtn.addEventListener("click", payLoan);


/*----------------------------LAPTOPS PART-----------------------------*/
const laptopsElement = document.getElementById("laptops");
const featuresList = document.getElementById("featuresList");
const laptopTitle = document.getElementById("laptopTitle");
const laptopDescription = document.getElementById("laptopDescription");
const laptopImg = document.getElementById("laptopImg");

let laptops = [];
let specs = [];

//fetch laptop data
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))

    const addLaptopsToMenu = (laptops) => {
        laptops.forEach(x => addLaptopToMenu(x));
        laptopTitle.innerText = laptops[0].title;
        laptopDescription.innerText = laptops[0].description;
        laptopImg.src = `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`;
        setAmount(laptops[0].price, laptopPrice);
        laptops[0].specs.forEach(x => specs.push(x));
        specs.forEach(handleSpecs);
    }

    //adds fetched laptops to select menu
    const addLaptopToMenu = (laptop) => {
        const laptopElement = document.createElement("option");
        laptopElement.value = laptop.id;
        laptopElement.appendChild(document.createTextNode(laptop.title));
        laptopsElement.appendChild(laptopElement);
    }

    //add the specs to ul as list items
    const handleSpecs = (x) => {
        let listItemElement = document.createElement("li");
        listItemElement.innerText = x;
        featuresList.appendChild(listItemElement)
    }

    //when user chooses laptop, update page with relevant data
    const handleLaptopMenuChange = e => {
        specs.length = 0;
        featuresList.innerHTML = "";
        const selectedLaptop = laptops[e.target.selectedIndex];
        selectedLaptop.specs.forEach(x => specs.push(x));
        specs.forEach(handleSpecs);
        laptopTitle.innerText = selectedLaptop.title;
        laptopDescription.innerText = selectedLaptop.description;
        setAmount(selectedLaptop.price, laptopPrice);
        laptopImg.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`;
    }

    //gets the index of chosen laptop and compares price to balance to decide if purchase is possible
    const buyLaptop = () => {
        chosenLaptopIndex = laptops.findIndex(x => x.title === laptopTitle.innerText);

        if(laptops[chosenLaptopIndex].price > balance){
            alert("You can't afford this laptop.");
        }else{
            balance -= laptops[chosenLaptopIndex].price;
            setAmount(balance, textAmount);
            alert(`Congratulations! You are now the owner of ${laptops[chosenLaptopIndex].title}.`);
        }
    }

    laptopsElement.addEventListener("change", handleLaptopMenuChange);
    buyBtn.addEventListener("click", buyLaptop);
